/**
 *
 */
package server;

import java.util.ArrayList;

/**
 * Liste des connexions actives.
 */
public class ActiveConnections {
    // Liste des connexions actives
    private ArrayList<ServerConnection> connectionsList = new ArrayList<>();

    /**
     * Diffusion d'un message à tous les clients connectés
     *
     * @param message message à diffuser
     */
    public synchronized void broadcast(String message) {
        System.out.println(message);
        for (ServerConnection connection : this.connectionsList
                ) {
            connection.sendToClient(message);
        }
    }

    /**
     * Ajout d'une nouvelle connexion.
     *
     * @param connection connexion à ajouter
     */
    synchronized void add(ServerConnection connection) {
        connectionsList.add(connection);
    }

    /**
     * Suppression d'une connexion.
     *
     * @param connection connexion à supprimer
     */
    synchronized void remove(ServerConnection connection) {
        connectionsList.remove(connection);
    }

    /**
     * Retourne une connexion à partir du pseudonyme du client associé. La
     * recherche ne sera pas sensible à la casse des pseudonymes.
     *
     * @param alias pseudonyme du client
     * @return connexion correspondant au pseudonyme, ou {@code null} si aucune
     * connexion n'existe pour ce pseudonyme
     */
    public synchronized ServerConnection get(String alias) {
        for (ServerConnection s: this.connectionsList
             ) {
            if(s.getAlias().toLowerCase().equals(alias.toLowerCase())) {
                return s;
            }
        }
        return null;
    }

    /**
     * Retourne la liste des pseudonymes des clients connectés
     *
     * @return pseudonymes séparés par des espaces
     */
    public synchronized String toString() {
        String string = "";
        for (ServerConnection connection : this.connectionsList) {
            string += connection.toString() + " ";
        }
        return string;
    }

}
