/**
 *
 */
package server;

import server.processors.MessageProcessor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe fournissant les fonctionnalités de communication avec un client. Une
 * instance de cette classe est créée chaque fois qu'un client se connecte.
 */
public class ServerConnection implements Runnable {

    // Objet chargé de traiter les messages provenant des clients
    private static MessageProcessor messageProcessor = new MessageProcessor();

    // socket de communication avec le client qui a ouvert cette connexion
    private Socket socket;

    // flot de sortie associé au socket
    private PrintStream output;

    // flot d'entrée associé au socket
    private BufferedReader input;

    // instance du serveur sur lequel cette connexion a été ouverte
    private ActiveConnections activeConnections;

    // état de la connexion
    private boolean active = false;

    // copie locale du pseudonyme du client
    private String alias = null;

    /**
     * Constructeur
     *
     * @param socket            socket de communication avec le client
     * @param activeConnections instance du serveur sur lequel cette connexion a été ouverte
     * @throws IOException en cas d'erreur lors de la création des flots d'entrée/sortie
     */
    public ServerConnection(Socket socket, ActiveConnections activeConnections) throws IOException {
        this.socket = socket;
        this.activeConnections = activeConnections;

        // Création du flot d'entrée associé à la connexion.
        // Ce qui est lu sur ce flot provient du serveur.
        this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        // Création du flot de sortie
        output = new PrintStream(socket.getOutputStream());
    }

    /*
     * Echange initial (lecture et validation du pseudonyme) et boucle de
     * communication avec le client.
     */
    @Override
    public void run() {
        // La boucle de communication avec le serveur sera donc exécutée dans
        // son propre thread.
        Executors.newSingleThreadExecutor().submit(() -> {
            try {
                // récupération du pseudonyme au client
                this.active = this.updateAlias(this.input.readLine());

                if (this.active) {
                    this.activeConnections.broadcast("#connected " + this.alias);
                    this.activeConnections.add(this);
                }

                // tant que la connexion est active
                while (active) {

                    // attente et lecture d'une ligne en provenance du serveur
                    String message = input.readLine();

                    // si le message est valide ...
                    if (message != null) {
                        // ... on le traite ...
                        messageProcessor.processMessage(message, this);
                    } else {
                        // sinon, cela signifie que le serveur a mis fin à la
                        // connexion
                        active = false;
                    }
                }
            } catch (IOException e) {
            }
            // avant de terminer, un peu de ménage...
            finally {
                try {
                    this.activeConnections.remove(this);
                    if (this.alias != null) {
                        this.activeConnections.broadcast("#disconnected " + this.alias);
                    }
                    // fermeture de la connexion
                    socket.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Envoi d'un message au client qui a ouvert cette connexion
     *
     * @param message message à envoyer
     */
    public void sendToClient(String message) {

        this.output.println(message);
    }

    /**
     * Retourne le pseudonyme du client.
     *
     * @return pseudonyme du client
     */
    public String getAlias() {
        return this.alias;
    }

    /**
     * Met à jour le pseudonyme du client. Si la mise à jour est effective, une
     * commande #alias est envoyée au client. Sinon, une commande #error est
     * émise.
     *
     * @param newAlias nouveau pseudonyme
     * @return true si la mise à jour a pu être effectuée, false sinon (newAlias
     * vaut null ou est déjà utilisé ou contient un caractère invalide)
     */
    public boolean updateAlias(String newAlias) {
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9_-]+$");
        Matcher matcher = pattern.matcher(newAlias);
        if (newAlias == null || !matcher.matches() || this.activeConnections.get(newAlias) != null) {
            this.sendToClient("#error invalid_alias");

            return false;
        } else {
            this.alias = newAlias;
            this.sendToClient("#alias " + newAlias);

            return true;
        }
    }

    /**
     * Retourne la liste des connexions actives.
     *
     * @return la liste des connexions actives.
     */
    public ActiveConnections getActiveConnections() {
        return activeConnections;
    }

    /**
     * désactive cette connexion.
     */
    public void stop() {
        this.active = false;
    }

    @Override
    public String toString() {
        return this.getAlias();
    }
}
