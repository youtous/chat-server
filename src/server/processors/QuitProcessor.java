package server.processors;

import server.ServerConnection;

/**
 * Created by youtous on 12/12/2016.
 */
public class QuitProcessor implements CommandProcessor {
    @Override
    public void processCommand(String args, ServerConnection connection) {
        connection.stop();
        connection.getActiveConnections().broadcast("#disconnected  " + connection.getAlias());
    }
}
