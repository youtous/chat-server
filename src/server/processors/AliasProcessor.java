package server.processors;

import server.ServerConnection;

/**
 * Created by youtous on 12/12/2016.
 */
public class AliasProcessor implements CommandProcessor {
    @Override
    public void processCommand(String args, ServerConnection connection) {
        String oldAlias = connection.getAlias();
        if (connection.updateAlias(args)) {
            connection.getActiveConnections().broadcast("#renamed " + oldAlias + " " + args);
        }
    }
}
