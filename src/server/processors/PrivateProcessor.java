package server.processors;

import server.ServerConnection;

import java.util.Arrays;

/**
 * Created by youtous on 12/12/2016.
 */
public class PrivateProcessor implements CommandProcessor {
    @Override
    public void processCommand(String args, ServerConnection connection) {
        String[] splittedArgs = args.split(" ");

        if (args.length() < 2) {
            connection.sendToClient("#error missing_argument");
        }
        ServerConnection serverConnection = connection.getActiveConnections().get(splittedArgs[0]);
        if (serverConnection != null) {
            String message = String.join(" ", Arrays.copyOfRange(splittedArgs, 1, splittedArgs.length));
            serverConnection.sendToClient("#private " + connection.getAlias() + " " + message);
        } else {
            connection.sendToClient("#error invalid_recipient");
        }
    }
}
