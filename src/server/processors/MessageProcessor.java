/**
 *
 */
package server.processors;

import java.util.Arrays;
import java.util.HashMap;

import server.ServerConnection;

/**
 * Classe utilisée par une connexion pour traiter les messages reçus du client
 */
public class MessageProcessor {
    /**
     * Processeurs de commandes.
     */
    private HashMap<String, CommandProcessor> commandProcessors;

    /**
     * Contructeur.
     */
    public MessageProcessor() {
        this.commandProcessors = new HashMap<>();
        this.commandProcessors.put("/alias", new AliasProcessor());
        this.commandProcessors.put("/quit", new QuitProcessor());
        this.commandProcessors.put("/list", new ListProcessor());
        this.commandProcessors.put("/private", new PrivateProcessor());
        this.commandProcessors.put("/ignore", new IgnoreProcessor());
        this.commandProcessors.put("/unignore", new UnignoreProcessor());
    }

    /**
     * Traite un message en provenance du client. En fonction de la nature de celui-ci (commande,
     * message "normal), différents traitements seront effectués
     *
     * @param message    message à traiter
     * @param connection connexion ayant reçu le message
     */
    public void processMessage(String message, ServerConnection connection) {
        message = message.trim();

        if (message.startsWith("/")) {
            String[] command = message.split(" ");

            System.out.println(String.join(" ", Arrays.copyOfRange(command, 1, command.length)));

            CommandProcessor commandProcessor = this.commandProcessors.get(command[0]);

            if (commandProcessor != null) {
                commandProcessor.processCommand(String.join(" ", Arrays.copyOfRange(command, 1, command.length)), connection);
            } else {
                connection.sendToClient("#error invalid_command");
            }
        } else {
            message = connection.getAlias() + "> " + message;
            connection.getActiveConnections().broadcast(message);
        }
    }
}
