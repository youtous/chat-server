package server.processors;

import server.ServerConnection;

import java.util.Arrays;

/**
 * Created by youtous on 12/12/2016.
 */
public class IgnoreProcessor implements CommandProcessor {
    @Override
    public void processCommand(String args, ServerConnection connection) {
        if (args.length() == 0) {
            connection.sendToClient("#error missing_argument");
        } else {
            ServerConnection target = connection.getActiveConnections().get(args);

            if (target == null) {
                connection.sendToClient("#error user_not_found");
            } else {
                connection.sendToClient("#ignore " + target.getAlias());
                target.sendToClient("#ignoredby " + connection.getAlias());
            }
        }
    }
}
